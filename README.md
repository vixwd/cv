# Badges

[![Netlify Status](https://img.shields.io/netlify/0f54b582-d9c3-4a7c-bc49-0084546702b9)](https://app.netlify.com/sites/vixwd/deploys)
[![Website Status](https://img.shields.io/website?url=https%3A%2F%2Fwww.vixwd.com%2F)](https://www.vixwd.com/)
[![Mozilla Status](https://img.shields.io/mozilla-observatory/grade-score/www.vixwd.com?publish)](https://observatory.mozilla.org/analyze/vixwd.com)
[![Header  Status](https://img.shields.io/security-headers?url=https%3A%2F%2Fwww.vixwd.com%2F)](https://securityheaders.com/?q=vixwd.com&followRedirects=on)

# Vixwd

Vixwd.com is a portfolio page for V.Drennen.
